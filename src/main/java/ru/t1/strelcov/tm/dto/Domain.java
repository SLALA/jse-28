package ru.t1.strelcov.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.model.*;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "domain")
@NoArgsConstructor
@Setter
@Getter
public class Domain implements Serializable {

    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JsonProperty("user")
    @JacksonXmlElementWrapper(localName = "users")
    @Nullable
    private List<User> users;

    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JsonProperty("task")
    @JacksonXmlElementWrapper(localName = "tasks")
    @Nullable
    private List<Task> tasks;

    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JsonProperty("project")
    @JacksonXmlElementWrapper(localName = "projects")
    @Nullable
    private List<Project> projects;

}
