package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IProjectTaskService;
import ru.t1.strelcov.tm.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-from-project";
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        @NotNull final Task task = projectTaskService.unbindTaskFromProject(userId, taskId);
        showTask(task);
    }

}
