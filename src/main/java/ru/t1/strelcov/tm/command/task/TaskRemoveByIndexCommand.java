package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(userId, index);
    }

}
