package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.IPropertyService;
import ru.t1.strelcov.tm.command.AbstractCommand;

public final class DisplayAboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println(propertyService.getName());
        System.out.println(propertyService.getEmail());
    }

}
