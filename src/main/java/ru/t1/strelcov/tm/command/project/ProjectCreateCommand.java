package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(userId, name, description);
        showProject(project);
    }

}
