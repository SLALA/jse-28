package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectFindByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-find-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Find project by name.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[FIND TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(userId, name);
        showProject(project);
    }

}
