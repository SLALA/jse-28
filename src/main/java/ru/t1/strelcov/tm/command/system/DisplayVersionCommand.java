package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IPropertyService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.command.AbstractCommand;

public final class DisplayVersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        System.out.println("[VERSION]");
        System.out.println(propertyService.getVersion());
    }

}
