package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public final class TaskListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "List tasks.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + ":");
        @NotNull List<Task> tasks;
        final String sort = TerminalUtil.nextLine();
        if (sort.isEmpty()) tasks = taskService.findAll(userId);
        else {
            if (SortType.isValidByName(sort)) {
                @NotNull final SortType sortType = SortType.valueOf(sort);
                @NotNull final Comparator comparator = sortType.getComparator();
                tasks = taskService.findAll(userId, comparator);
                System.out.println(sortType.getDisplayName());
            } else
                throw new IncorrectSortOptionException(sort);
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
