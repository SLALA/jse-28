package ru.t1.strelcov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User removeByLogin(@NotNull String login);

    boolean loginExists(@NotNull final String login);

}
