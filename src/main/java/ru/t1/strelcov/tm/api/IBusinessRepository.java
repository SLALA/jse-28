package ru.t1.strelcov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    @NotNull
    List<E> findAll(@NotNull final String userId);

    @NotNull
    List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator);

    @Nullable
    E findByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    E findByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    E removeByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    E removeByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    E findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    E removeById(@NotNull final String userId, @NotNull final String id);

    void clear(@NotNull final String userId);

}
