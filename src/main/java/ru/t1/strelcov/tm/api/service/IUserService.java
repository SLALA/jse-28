package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User add(@Nullable String login, @Nullable String password);

    @NotNull
    User add(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User add(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User findByLogin(@Nullable String name);

    @NotNull
    User removeByLogin(@Nullable String name);

    @NotNull
    User updateById(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    User updateByLogin(@Nullable String login, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    void changePasswordById(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
