package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public void add(@NotNull final E entity) {
        list.add(entity);
    }

    @Override
    public void addAll(@NotNull final List<E> entities) {
        list.addAll(entities);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void remove(@NotNull final E entity) {
        list.remove(entity);
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return list.stream().filter((e) -> id.equals(e.getId())).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
