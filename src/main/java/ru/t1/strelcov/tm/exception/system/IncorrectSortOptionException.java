package ru.t1.strelcov.tm.exception.system;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class IncorrectSortOptionException extends AbstractException {

    public IncorrectSortOptionException(final String value) {
        super("Error: Input value \"" + value + "\" is not valid sort option.");
    }

}
